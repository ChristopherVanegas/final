# creamos nuestra clase planilla
class Nomina:
    # iniciamos nuestra clase
    def __init__(self):
        # crearemos una lista donde guardaremos los datos de nuestra agenda
        self.empleados = []

    # menu del programa
    def menu(self):
        print()
        menu = [
            ['--------------------------------------- MenÃº Principal de Planilla Quincenal ------------------------------------------\n'],
            ['                                       1. Introducir Datos\n', "anadir"],
            ['                                       2. Generar Borrador\n'],
            ['                                       3. Buscar empleado\n'],
            ['                                       4. Editar Datos\n'],
            ['                                       5. Cerrar \n']
        ]

        for x in range(6):
            print(menu[x][0])

        opcion = int(input("Introduzca la opciÃ³n deseada: "))
        if opcion == 1:
            self.anadir()
        elif opcion == 2:
            self.lista()
        elif opcion == 3:
            self.buscar()
        elif opcion == 4:
            self.editar()
        elif opcion == 5:
            print("Saliendo ...")
            exit()

        # volvemos a llamar al menÃº
        self.menu()






    # funciÃ³n para aÃ±adir un empleado3
    # Tambien calcularemos las deducciones legales y los salarios quincenales para posteriormente ser impresos por pantalla
    def anadir(self):
        print("------------------------------------------------------")
        print("             AÃ±adir Nuevo Empleado e Incidencias                   ")
        print("-------------------------------------------------------")
        nom = input("Introduzca el nombre: ")
        salxh = float(input("Introduzca el Salario por Hora del Empleado: "))
        hb = int(input("Introduzca las horas quincenales del Empleado: "))
        dep=input("Introduzca el nombre del Departamento de la persona")
        email = input("Introduzca el email: ")
        sm=salxh*hb
        ss=sm*0.0975
        se=sm*0.0125
        tardanzas=float(input("Introduzca la cantidad de horas a descontar  :"))
        ttardanzas=tardanzas*salxh
        psalm=sm*2
        psaly=psalm*13
        if psaly>=11000:

            rentg=psaly-11000
            rentg2=rentg*0.15
            isrmes=rentg2/13
            isrqui=isrmes/2

        else:
            rentg = 0
            rentg2 = 0
            isrmes = 0
            isrqui=0

        saln=sm-ss-se-isrqui-ttardanzas



        self.empleados.append({'nombre': nom, 'rxh': salxh, 'Horas Base':hb,  'dep':dep, 'email': email,
                               'Salario Mensual':sm, 'Seguro Social':ss, 'Seguro Educativo':se,'tardanzas':tardanzas
                                ,'ttardanzas':ttardanzas,'psalm':psalm,'psaly':psaly,'rentg': rentg,'rentg2':rentg2,
                               'isrmes':isrmes,'isrqui':isrqui,'saln':saln})








    # funciÃ³n para calcular la planilla de manera parcial
    # En este caso imprimiremos Nombre, salario, salario por quincena, deducciones basicas
    # con ellos podremos buscar luego un empleado
    def lista(self):
        print("------------------")
        print("-----------------------------------------------Calcular Planilla Parcial-----------------------------------------------------")
        if len(self.empleados) == 0:
            print("No existen empleados disponibles")
        else:
            print("Nombre              Rata     |Salario Mensual |  Seguro Social|  Seguro Educativo| ISR   | AusNoPag | NETO")
            for x in range(len(self.empleados)):


                print(self.empleados[x]['nombre'],
                      "               ""{0:.2f}".format(self.empleados[x]['rxh']),
                      "      ""{0:.2f}".format(self.empleados[x]['Salario Mensual']),
                      "            ""{0:.2f}".format(self.empleados[x]['Seguro Social']),
                      "       4  ""{0:.2f}".format(self.empleados[x]['Seguro Educativo']),
                      "            ""{0:.2f}".format(self.empleados[x]['isrqui']),
                      "     ""{0:.2f}".format(self.empleados[x]['ttardanzas']),
                      "  ""{0:.2f}".format(self.empleados[x]['saln']))








    # funciÃ³n para buscar un empleado a travÃ©s del nombre
    def buscar(self):
        print("---------------------")
        print("Buscador de Empleados")
        print("---------------------")
        nom = input("Introduzca el nombre del Empelado: ")
        for x in range(len(self.empleados)):
            if nom == self.empleados[x]['nombre']:
                print("Datos del EMPLEADO")
                print("Nombre: ", self.empleados[x]['nombre'])
                print("Salario por hora: ", self.empleados[x]['rxh'])
                print("horas base ", self.empleados[x]['Horas Base'])
                return x







    # funciÃ³n para editar los datos Principales del empleado
    def editar(self):
        print("---------------")
        print("Editar empleado")
        print("---------------")
        data = self.buscar()
        condition = False
        while condition == False:
            print("Selecciona que desea editar: ")
            print("1. Nombre")
            print("2. Rata por Hora")
            print("3. Horas Base")
            print("4. Volver")
            option = int(input("Introduzca la opciÃ³n deseada: "))
            if option == 1:
                nom = input("Introduzca el nuevo nombre: ")
                self.empleados[data]['nombre'] = nom
            elif option == 2:
                rxh = float(input("Introduzca la nueva rata por hora: "))
                self.empleados[data]['rxh'] = rxh
            elif option == 3:
                hb = int (input("introduzca las horas base correctas: "))
                self.empleados[data]['Horas Base'] = hb
            elif option == 4:
                condition = True


# bloque principal
nomina = Nomina()
nomina.menu()